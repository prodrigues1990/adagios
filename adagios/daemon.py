# Adagios is a web based Nagios configuration interface
#
# Copyright (C) 2014, Tomas Edwardsson <tommi@tommi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Methods for controlling and getting status of the Nagios daemon"""

from pynag.Control import daemon
from pynag.Utils import runCommand
from adagios import settings

class Daemon(daemon):
    def __init__(self):
        super().__init__(
            nagios_bin=settings.nagios_binary,
            nagios_cfg=settings.nagios_config,
            nagios_init=settings.nagios_init_script,
            service_name=settings.nagios_service,
        )

    def verify_config(self):
        """
        Run nagios -v config_file to verify that the conf is working
        :returns:   True -- if pynag.Utils.runCommand() returns 0, else None
        """
        if self.sudo:
            cmd = [self.nagios_bin, "--allow-root", "-v", self.nagios_cfg]
        else:
            cmd = [self.nagios_bin, "-v", self.nagios_cfg]

        result, self.stdout, self.stderr = runCommand(cmd, shell=False)

        if result == 0:
            return True
        else:
            return None

# vim: sts=4 expandtab autoindent
